package com.chenhj.customEnum;

public enum Znx {
	
	//江苏银行 productId = "3";
	//中国银行 productId = "18";
	//招商银行 productId = "29";
	//浙商银行 productId = "?"; //TODO
	ZSYH("wzsy_ysx_znx_zsyh","100","浙商银行"),
	JSYH("wzsy_ysx_znx_jsyh","3","江苏银行"),
	ZHASYH("wzsy_ysx_znx_zhasyh","29","招商银行"),
	ZGYH("wzsy_ysx_znx_zgyh","18","中国银行");
	
	private String tableName;	
	private String productId;
	private String bankName;
	
	public String getTableName() {
		return tableName;
	}

	public String getProductId() {
		return productId;
	}
	
	public String getBankName() {
		return bankName;
	}

	private Znx(String tableName, String productId, String bankName) {
		this.tableName = tableName;
		this.productId = productId;
		this.bankName = bankName;
	}
	
}
