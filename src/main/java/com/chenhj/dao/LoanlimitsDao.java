package com.chenhj.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.chenhj.entity.Loanlimits;

/**
 * @author chenhj
 *
 */
public interface LoanlimitsDao {
	
	List<Loanlimits> queryAll(@Param("offset") int offset, @Param("limit") int limit,@Param("whether") int whether,@Param("tableName") String tableName);
	
	int getCount(@Param("tableName") String tableName);
	
	void addFailuredLoanlimits(Loanlimits loanlimits);
}
 