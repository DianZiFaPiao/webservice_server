package com.chenhj.dto;


import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("content")  
public class Content {
	private String text;
	private String title;
	private String msgtype;
	private String url;
	private String signname;
	private String areacode;
	private String pushdesc;
	private String data;
	private String biztype;
	private String subbiztype;
	private String usetemplate;
	private String recipients;
	private String nsrdzdah;
	private String sendtime;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMsgtype() {
		return msgtype;
	}
	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getSignname() {
		return signname;
	}
	public void setSignname(String signname) {
		this.signname = signname;
	}
	public String getAreacode() {
		return areacode;
	}
	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}
	public String getPushdesc() {
		return pushdesc;
	}
	public void setPushdesc(String pushdesc) {
		this.pushdesc = pushdesc;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getBiztype() {
		return biztype;
	}
	public void setBiztype(String biztype) {
		this.biztype = biztype;
	}
	public String getSubbiztype() {
		return subbiztype;
	}
	public void setSubbiztype(String subbiztype) {
		this.subbiztype = subbiztype;
	}
	public String getUsetemplate() {
		return usetemplate;
	}
	public void setUsetemplate(String usetemplate) {
		this.usetemplate = usetemplate;
	}
	public String getRecipients() {
		return recipients;
	}
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}
	public String getNsrdzdah() {
		return nsrdzdah;
	}
	public void setNsrdzdah(String nsrdzdah) {
		this.nsrdzdah = nsrdzdah;
	}
	public String getSendtime() {
		return sendtime;
	}
	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}
	@Override
	public String toString() {
		return "Content [text=" + text + ", title=" + title + ", msgtype=" + msgtype + ", url=" + url + ", signname="
				+ signname + ", areacode=" + areacode + ", pushdesc=" + pushdesc + ", data=" + data + ", biztype="
				+ biztype + ", subbiztype=" + subbiztype + ", usetemplate=" + usetemplate + ", recipients=" + recipients
				+ ", nsrdzdah=" + nsrdzdah + ", sendtime=" + sendtime + "]";
	}
	
	
	
	
	
}
