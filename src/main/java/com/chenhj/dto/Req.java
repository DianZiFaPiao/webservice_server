package com.chenhj.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("req")  
public class Req {
	
	//将id设置为XML req 元素�? attribute  
    @XStreamAsAttribute
	private String id;
    //将channel设置为XML req 元素�? attribute  
    @XStreamAsAttribute
	private String channel;
	private String act;
	private String body;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getAct() {
		return act;
	}
	public void setAct(String act) {
		this.act = act;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	@Override
	public String toString() {
		return "Req [id=" + id + ", channel=" + channel + ", act=" + act + ", body=" + body + "]";
	}
	
	
}
