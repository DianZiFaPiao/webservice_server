package com.chenhj.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("resp")  
public class Resp {
	private String result;
	private String msg;
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	@Override
	public String toString() {
		return "Resp [result=" + result + ", msg=" + msg + "]";
	}
	
	
	
	
	
	
}
