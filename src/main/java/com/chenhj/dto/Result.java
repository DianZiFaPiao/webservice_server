package com.chenhj.dto;

public class Result<T> {
	
	private String code;
	private String msg;
	private T content;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public T getContent() {
		return content;
	}
	public void setContent(T content) {
		this.content = content;
	}
	
	
	public Result() {
	}
	
	
	public Result(String code, String msg, T content) {
		super();
		this.code = code;
		this.msg = msg;
		this.content = content;
	}
	@Override
	public String toString() {
		return "Result [code=" + code + ", msg=" + msg + ", content=" + content + "]";
	}
	
	
}
