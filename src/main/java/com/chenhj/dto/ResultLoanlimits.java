package com.chenhj.dto;

import java.util.List;

import com.chenhj.entity.Loanlimits;

public class ResultLoanlimits {
	
	private String code;
	private String msg;
	private List<Loanlimits> loanlimits;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<Loanlimits> getLoanlimits() {
		return loanlimits;
	}
	public void setLoanlimits(List<Loanlimits> loanlimits) {
		this.loanlimits = loanlimits;
	}
	public ResultLoanlimits() {
		super();
	}
	public ResultLoanlimits(String code, String msg, List<Loanlimits> loanlimits) {
		super();
		this.code = code;
		this.msg = msg;
		this.loanlimits = loanlimits;
	}
	
	
}
