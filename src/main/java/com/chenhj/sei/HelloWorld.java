package com.chenhj.sei;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.chenhj.dto.Result;
import com.chenhj.dto.ResultLoanlimits;

@WebService
public interface HelloWorld {

	@WebMethod
	String sayHello(String name);
	@WebMethod
	Result<Integer> getCount(String tableName);
	@WebMethod
	ResultLoanlimits getLoanlimits(int offset,int limit,int whether,String tableName);
	
	
}
