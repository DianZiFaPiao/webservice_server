package com.chenhj.sei.imp;


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.chenhj.dao.LoanlimitsDao;
import com.chenhj.dto.Result;
import com.chenhj.dto.ResultLoanlimits;
import com.chenhj.sei.HelloWorld;
import com.chenhj.service.imp.LoanlimitsServiceImp;

@WebService
public class HelloWorldWS implements HelloWorld {

	//无法自动注入
	// webService注解的类初始化优先于ioc容器中bean的初始化
	
	private LoanlimitsServiceImp loanlimitsServiceImp;
	
	private LoanlimitsDao loanlimitsDao;
	
	public LoanlimitsDao getLoanlimitsDao() {
		return loanlimitsDao;
	}

	public void setLoanlimitsDao(LoanlimitsDao loanlimitsDao) {
		this.loanlimitsDao = loanlimitsDao;
	}

	public LoanlimitsServiceImp getLoanlimitsServiceImp() {
		return loanlimitsServiceImp;
	}

	public void setLoanlimitsServiceImp(LoanlimitsServiceImp loanlimitsServiceImp) {
		this.loanlimitsServiceImp = loanlimitsServiceImp;
	}

	@WebMethod
	public Result<Integer> getCount(@WebParam(name="tablename") String tableName) {
		return new Result<Integer>("0000", "SUCCESS", loanlimitsServiceImp.getCount(tableName));
	}
	
	@WebMethod
	public String sayHello(@WebParam(name="name") String name) {
		return name + ",你好！！！";
	}

	@Override
	public ResultLoanlimits getLoanlimits(@WebParam(name="offset") int offset, @WebParam(name="limit") int limit, @WebParam(name="whether") int whether, @WebParam(name="tableName") String tableName) {
		
		return new ResultLoanlimits("0000", "SUCCESS", loanlimitsDao.queryAll(offset, limit, whether, tableName));
	}

}
