package com.chenhj.service;

import java.util.List;


public interface LoanlimitsService {
	
	List<String> postXml(int offset,int limit,int whether,String tableName,String productId,String bankName);
	
	int getCount(String tableName);
	
	void addFailuredLoanlimits(String reqXml,String tableName);
}
