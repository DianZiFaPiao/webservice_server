package com.chenhj.utils;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;


public class EncryptUtil {
	/**
	 * 
	 * @param oldStr 需要加密的 byte[] or char[] or String
	 * @param algorithmName 加密算法 MD5 or SHA1
	 * @param hashIterations 加密次数
	 * @return
	 */

	public static String strToEncrypt(Object oldStr,String algorithmName,int hashIterations){
		SimpleHash simpleHash = new SimpleHash(algorithmName, oldStr, null, hashIterations);
		return simpleHash.toString();
	}
	
	/**
	 * 
	 * @param oldStr
	 * @param algorithmName
	 * @param hashIterations
	 * @param salt 盐值
	 * @return
	 */
	public static String strToEncrypt(Object oldStr,String algorithmName,int hashIterations,String salt){
		
		ByteSource credentialsSalt = ByteSource.Util.bytes(salt);  
		SimpleHash simpleHash = new SimpleHash(algorithmName, oldStr, credentialsSalt, hashIterations);
		return simpleHash.toString();
	}
	
	public static void main(String[] args) {
		String result = "chenhj";
		String resultStr = strToEncrypt(result,"MD5",1024);
		String resultStrSalt = strToEncrypt(result,"MD5",1024,"chenhj");
		System.out.println(resultStr);
		System.out.println(resultStrSalt);
		
	}
}
